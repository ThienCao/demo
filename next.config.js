/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  swcMinify: true,
  output: 'standalone',
  // experimental: {
  //   outputStandalone: true
  // },
  // trailingSlash: true,
  eslint: {ignoreDuringBuilds: true},
  images: {
    domains: ['localhost', 'vsports-files.ss-hn-1.bizflycloud.vn', 'hcm01.vstorage.vngcloud.vn','i.imgur.com'],
    minimumCacheTTL: 60
  },
  async rewrites() {
    return [{source: '/apis/:path*', destination: '/api/:path*'}]
  }
}

module.exports = nextConfig
