import {
  Button,
  Flex,
  Grid,
  GridItem,
  Input,
  InputGroup,
  InputRightElement,
  Link,
  Text,
} from "@chakra-ui/react";
import React, { useState } from "react";
import styles from "../../styles/Home.module.css";
import Head from "next/head";
import * as usersAPI from "../../api/user";

const User = ({ data }) => {
  const [state, setState] = useState({});
  const [loading, setLoading] = useState(false);

  React.useEffect(() => {
    fetchData();
  }, []);
  const fetchData = async () => {
    try {
      if (loading) return;
      setLoading(true);
      const data = await usersAPI.getUserById("1");
      setState(data);
      setLoading(false);
    } catch (e) {
      setLoading(false);
    }
  };
  return (
    <Flex
      className={styles.container}
      width="100%"
      justifyContent="center"
      alignItems="center"
      flexDirection="column"
    >
      <Text className={styles.title}>Edit User</Text>
      <Link href="https://beta.vstation.vn/users/1/edit" target="black">
        <Text>Click here to see user edit page at vsports</Text>
      </Link>
      <Flex
        className={styles.card}
        width="50%"
        justifyContent="center"
        alignItems="center"
      >
        <Grid templateColumns="repeat(2, 1fr)" flexDirection="center" gap={6}>
          <GridItem w="100%">
            <Flex width="100%" direction="column">
              <Text>name:</Text>
              <InputGroup my={2}>
                <Input />
              </InputGroup>
            </Flex>
          </GridItem>
          <GridItem w="100%">
            <Flex width="100%" direction="column">
              <Text>ngày sinh:</Text>
              <InputGroup my={2}>
                <Input type="date" />
              </InputGroup>
            </Flex>
          </GridItem>{" "}
          <GridItem w="100%">
            <Flex width="100%" direction="column">
              <Text>SDT:</Text>
              <InputGroup my={2}>
                <Input />
              </InputGroup>
            </Flex>
          </GridItem>{" "}
          <GridItem w="100%">
            <Flex direction="column">
              <Text>Email:</Text>
              <InputGroup my={2}>
                <Input />
              </InputGroup>
            </Flex>
          </GridItem>
          <GridItem w="100%">
            <Flex direction="column">
              <Text>Số CMND/CCCD/Hộ Chiếu :</Text>
              <InputGroup my={2}>
                <Input />
              </InputGroup>
            </Flex>
          </GridItem>
          <GridItem w="100%">
            <Flex direction="column">
              <Text>Giới tính:</Text>
              <InputGroup my={2}>
                <Input />
              </InputGroup>
            </Flex>
          </GridItem>
        </Grid>
      </Flex>
      <Button isLoading={loading}>
        <Text>Submit</Text>
      </Button>
    </Flex>
  );
};

export default User;
