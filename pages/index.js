import Head from "next/head";
import styles from "../styles/Home.module.css";
import { Flex, Text } from "@chakra-ui/react";
import Link from "next/link";

export default function Home() {
  return (
    <Flex className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <Text className={styles.title}>
          Welcome to <a href="https://vsports.vn">Vsports.vn!</a>
        </Text>

        <Text className={styles.description}>
          Get started by editing{" "}
          <Text style={{ color: "blue", fontSize: 20, fontWeight: "bold" }}>
            information of user
          </Text>
        </Text>

        <Flex cursor="pointer" className={styles.grid}>
        <Link href={'user'} className={styles.card}>
            {/* <Text>Documentation &rarr;</Text> */}
            <Text>
              Click here to go to the user edit page
            </Text>
          </Link>
        </Flex>
      </main>
    </Flex>
  );
}
