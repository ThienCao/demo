import axios from "../axios";

export function getUserById(id) {
  return axios({
    baseURL: "https://beta.vstation.vn",
    url: `/api/v1/users/${id}`,
    method: "get",
  }).then((res) => res.data);
}

export function updateUser(data) {
  return axios({
    baseURL: "https://beta.vstation.vn",
    url: `/api/v1/users`,
    method: "put",
    data,
  }).then((res) => res.data);
}
