import axios from "axios";
import { getCookie, setCookie } from "cookies-next";

import config from "../constants/config";
// import config from 'constants/config'

// Create axios instance.
const axiosInstance = axios.create({
  // baseURL: config.baseURL,
  baseURL: "https://beta.vstation.vn/",
  timeout: 30000,
  // withCredentials: true,
});

let accessToken = "";
let refreshToken = "";
let isRefreshing = false;
let lang = "vi";
let refreshSubscribers = [];

export const setRefreshToken = (tk) => {
  refreshToken = tk;
};
export const setAccessToken = (tk) => {
  accessToken = tk;
};
export const setLang = (_lang) => {
  lang = _lang;
};

const subscribeTokenRefresh = (cb) => {
  refreshSubscribers.push(cb);
};
export const setToken = ({ access, refresh }) => {
  if (access) accessToken = access;
  if (refresh) refreshToken = refresh;
};
export const setTokenFromServer = (ctx) => {
  const _accessToken = getCookie("accessToken", ctx);
  const _refreshToken = getCookie("refreshToken", ctx);
  if (_accessToken) accessToken = _accessToken;
  if (_refreshToken) refreshToken = _refreshToken;
};
const onRefreshed = (token) => {
  refreshSubscribers.forEach((cb) => cb(token));
};

// Request interceptor for API calls
axiosInstance.interceptors.request.use(
  async (config) => {
    config.headers = {
      ...config.headers,
      Authorization: `Bearer ${
        getCookie("accessToken") ||
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjEiLCJuYW1lIjoiTmd1eWVuIEhpZW4gVmluaCIsImF2YXRhciI6Imh0dHBzOi8vdnNwb3J0cy5ibG9iLmNvcmUud2luZG93cy5uZXQvZmlsZXMvMTY5MzMwMzcwNjM0OS1SVnBhLmpwZWciLCJ0eXBlIjoiYWRtaW4iLCJsYW5nSWQiOiJ2aSIsImJpcnRoZGF5IjoiMTk5NS0wNy0xOFQxNzowMDowMC4wMDBaIiwiZW1haWwiOiJ0aGllbmJybzE5OTdAZ21haWwuY29tIiwicGhvbmUiOiIwMzM0Njk1MDA4IiwiaWF0IjoxNjk2MjQwNjEzLCJleHAiOjE3Mjc3OTgyMTN9.lZ7w5CYHiEvjpPBmnvo6804wZS4nxAJTkD9J-6miUNo"
      }`,
    };
    config.params = config.params || {};
    config.params["lang"] = lang;
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

// Response interceptor for API calls
axiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  async function (error) {
    const originalRequest = error.config;

    if (
      error?.response?.status === 401 &&
      originalRequest?.url?.indexOf("/signin") === -1 &&
      !originalRequest._retry
    ) {
      originalRequest._retry = true;
      if (!isRefreshing) {
        isRefreshing = true;
        axios({
          baseURL: config.baseURL,
          url: "/api/v1/users/refresh-token",
          method: "post",
          data: { token: refreshToken || getCookie("refreshToken") },
        })
          .then((res) => res.data)
          .then((res) => {
            accessToken = res.accessToken;
            refreshToken = res.refreshToken;

            isRefreshing = false;
            onRefreshed(accessToken);
          })
          .catch((e) => Promise.reject(e));
      }
      const retryOrigReq = new Promise((resolve, reject) => {
        subscribeTokenRefresh((token) => {
          // replace the expired token and retry
          originalRequest.headers["Authorization"] = "Bearer " + token;
          resolve(axios(originalRequest));
        });
      });
      return retryOrigReq;
    }
    return Promise.reject(
      Array.isArray(error?.response?.data?.errors)
        ? error?.response?.data
        : error
    );
  }
);

export default axiosInstance;
