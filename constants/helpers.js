import React from 'react'
import CryptoJS from 'crypto-js'
import {toastRef} from 'context'
import {ToastMessage} from 'components/ToastMessage'
import {setCookie} from 'cookies-next'
import {useTranslation} from 'context'
// import axios from 'axios'
import Swal from 'sweetalert2'
import moment from 'moment'

export const concat = (preArray, nextArray, key) => {
  return [...nextArray, ...preArray].filter(
    (
      s => a =>
        !s.has(a[key] || a['id'] || a['_id']) && s.add(a[key] || a['id'] || a['_id'])
    )(new Set())
  )
}

/**
 * Hiện thị Alert (Toast message)
 *
 * @param {string} type  Loại của Alert, bao gồm : 'success', 'error', 'warning', 'info'
 * @param {string} message Message muốn hiện thị: Key ngôn ngữ hoặc ID MESSAGE.
 */
export const showAlert = (type, message) => {
  return toastRef.current({
    position: 'top-right',
    duration: 2000,
    render: ({onClose}) => <ToastMessage title={message} onClose={onClose} type={type} />,
    isClosable: true
  })
}

export const sort = (arr, key) => {
  return arr.sort((d1, d2) => new Date(d2[key || 'id' || '_id']).getTime() - new Date(d1[key || 'id' || '_id']).getTime())
}

export const numberFormat = n => {
  if (n < 1e3) return n
  if (n >= 1e3 && n < 1e6) return +(n / 1e3).toFixed(1) + 'K'
  if (n >= 1e6 && n < 1e9) return +(n / 1e6).toFixed(1) + 'M'
  if (n >= 1e9 && n < 1e12) return +(n / 1e9).toFixed(1) + 'B'
  if (n >= 1e12) return +(n / 1e12).toFixed(1) + 'T'
}

export const sliceText = (text, limit) => {
  if (text?.length > limit)
    for (let i = limit; i > 0; i--) {
      if (text.charAt(i) === ' ' && (text.charAt(i - 1) != ',' || text.charAt(i - 1) != '.' || text.charAt(i - 1) != ';')) {
        return text.substring(0, i) + '...'
      }
    }
  else return text
}

export const debounce = (fn, delay) => {
  let timer = null
  return function () {
    const context = this,
      args = arguments
    clearTimeout(timer)
    timer = setTimeout(() => {
      fn.apply(context, args)
    }, delay)
  }
}
// Decrypt