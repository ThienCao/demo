import {extendTheme, withDefaultColorScheme} from '@chakra-ui/react'
import {mode} from '@chakra-ui/theme-tools'

const customTheme = extendTheme(
  {
    styles: {
      global: props => ({
        'html, body': {
          background: mode('#f2f5f6', '#18191A')(props) //mode(light mode color, dark mode color)
        }
      })
    },
    shadows: {outline: 'none'},
    colors: {
      brand: {
        100: '#ffe0b2',
        200: '#ffcc80',
        300: '#ffb74d',
        400: '#ffa726',
        500: '#ff9800',
        600: '#fb8c00',
        700: '#f57c00',
        800: '#ef6c00',
        900: '#e65100'
      },
      brand2: {
        100: '#bbdefb',
        200: '#90caf9',
        300: '#64b5f6',
        400: '#42a5f5',
        500: '#2196f3',
        600: '#1e88e5',
        700: '#1976d2',
        800: '#1565c0',
        900: '#2196F3'
      },
      'theme.gray': '#f2f5f6',
      commentBgColor: '#F0F2F5'
    },
    components: {
      Text: {
        baseStyle: {
          fontSize: 14
          // fontWeight: '400',
          // color: '#757575'
        }
      },
      Heading: {
        baseStyle: {
          fontSize: 32,
          fontWeight: '500'
        }
      }
      // PopoverContent: {
      //   baseStyle: {
      //     track: {
      //       _focus: {
      //         boxShadow: 'none'
      //       }
      //     }
      //   }
      // }
    }
  },
  withDefaultColorScheme({
    colorScheme: 'brand2',
    components: ['Button']
  }),
  withDefaultColorScheme({
    colorScheme: 'red',
    components: ['Badge']
  })
)

export default customTheme
