// const SSR_BASE_URL = process.env.NODE_ENV === 'development' || typeof window !== 'undefined' ? 'https://vstation.vn' : 'http://172.17.0.1'
// const SSR_BASE_URL = typeof window === 'undefined' && process.env.NODE_ENV === 'production' ? 'http://172.17.0.1' : 'https://vstation.vn'
// const SSR_BASE_URL = typeof window === 'undefined' && process.env.NODE_ENV === 'production' ? 'http://172.17.0.1' : 'https://vstation.vn'
// const CSR_BASE_URL =  //process.env.NODE_ENV === 'development' ? process.env.CSR_BASE_URL : ''

export default {
  exchangeRate: 1000,
  numberOfFiles: 30,
  maxFileSize: 629145600,
  // baseURL: 'http://172.17.0.1',typeof window === 'undefined' && process.env.NODE_ENV === 'production' ? process.env.SSR_BASE_URL :
  baseURL: process.env.NODE_ENV === 'development' ? 'https://beta.vstation.vn' : process.env.BASE_URL,
  searchAPIKey: 'Lh2QuFYg9mSnrRhFfyDRvRjLwtdMe5zRGJNtrRGEYbAzSeAmjIUO',
  anonymousToken:
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwibmFtZSI6IkFub255bW91cyIsImF2YXRhciI6IiIsInR5cGUiOiJhbm9ueW1vdXMiLCJsYW5ndWFnZV9pZCI6ImVuIiwiaWF0IjoxNjUzMzQwOTE5fQ.9BMudg88cBjqhLzB1BAvg7SKgm1cSEbV04leVW-ety8',
  facebookAppId: '1020991372022040',
  googleClientId: '503591862458-76joohs0f0h2uqq2p9csutufe80jkr1g.apps.googleusercontent.com', //'157435564732-5h6og74qg34ui9rpvn332400i45dkgm8.apps.googleusercontent.com',
  apiUrl: 'https://api.vsports.com.vn',
  chatUrl: 'https://chat.vsports.com.vn',
  vtokenUrl: 'https://token.vsports.com.vn',
  logo: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/vstation.jpg',
  logoDefault: 'https://vsports.vn/LOGO-V-02.png',
  background: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/cover.jpg',
  coverSeason: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vstationfiles/L85OXrEFCT.png',
  error: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/error.png',
  empty: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/empty.png',
  downloadapp: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/mockup%20app.png',
  android: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/chplay%20download.png',
  ios: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/appstore%20download.png',
  upfile: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1632937750631.png',
  icons: {
    facebook: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/icon_faceb_fbyrl.png',
    instagram: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/instagram__moifb.png',
    // behance: 'https://icons-for-free.com/iconfiles/png/512/part+2+behance-1320568349966413788.png',
    twitter: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1653473885395.png',
    youtube: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1653473885682.png',
    tiktok: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1653473885126.png',
    linkedin: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1653473884818.png',
    website: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vstationfiles/59qE3Btlf1.png'
  },
  icon_youtube: 'https://iconarchive.com/download/i67995/marcus-roberto/google-play/YouTube.ico',
  icon_be: 'https://icons-for-free.com/iconfiles/png/512/part+2+behance-1320568349966413788.png',
  icon_facebook_white: 'https://toppng.com/uploads/preview/facebook-logo-white-white-facebook-f-logo-115628618682gxdsl4yn5.png',
  icon_instagram_white: 'https://toppng.com/uploads/preview/white-instagram-icon-instagram-logo-instagram-instagram-icon-white-11553385558clvu7hxuql.png',
  icon_linkedin_white: 'https://img.favpng.com/17/10/21/black-and-white-point-angle-pattern-png-favpng-87tBBC3RLZRE66LcjpZ2FbqNU_t.jpg',
  icon_twitter_white: 'https://toppng.com/uploads/preview/twitter-icon-white-transparent-11549537259z0sowbg17j.png',
  icon_youtube_white: 'https://toppng.com/uploads/preview/youtube-logo-png-white-11618670025bypo1gtbje.png',
  icon_tiktok_white: 'https://image.pngaaa.com/645/4768645-small.png',
  icon_facebook: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/icon_faceb_fbyrl.png',
  icon_instagram: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/instagram__moifb.png',
  icon_linkedin: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1653473884818.png',
  icon_twitter: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1653473885395.png',
  icon_tiktok: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1653473885126.png',
  national_id_front:
    'https://upload.wikimedia.org/wikipedia/commons/thumb/9/96/C%C4%83n_c%C6%B0%E1%BB%9Bc_c%C3%B4ng_d%C3%A2n_g%E1%BA%AFn_ch%C3%ADp_m%E1%BA%B7t_tr%C6%B0%E1%BB%9Bc.jpg/500px-C%C4%83n_c%C6%B0%E1%BB%9Bc_c%C3%B4ng_d%C3%A2n_g%E1%BA%AFn_ch%C3%ADp_m%E1%BA%B7t_tr%C6%B0%E1%BB%9Bc.jpg',
  national_id_back: 'https://bcp.cdnchinhphu.vn/Uploaded/phungthithuhuyen/2021_02_03/image007.jpg',
  icon_football: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/Vector_loffn.png',
  icon_volleyball: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/Basketball_gxqrx.png',
  icon_basketball: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/Volleyball_wvsyb.png',
  icon_tennis: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/Union_esdvo.png',
  icon_bicycle: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1654745237603.png',
  icon_athletics: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1654745237868.png',
  icon_music: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1654746201262.png',
  icon_diamond_medal: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1656555239284.png',
  icon_silver_medal: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1656646976015.png',
  icon_gold_medal: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1656646976312.png',
  icon_broze_medal: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1656647928262.png',
  icon_media_medal: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1657682662442.png',
  icon_other_medal: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1657682662246.png',
  reactions: [
    {
      id: 'like',
      description: 'Like',
      img: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/like.gif',
      ic: 'https://i.ibb.co/THM117v/flat-icon-png-40278.png',
      color: '#2196f3'
    },
    {
      id: 'love',
      description: 'Love',
      img: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/love.gif',
      ic: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/love2.png',
      color: '#e91e63'
    },
    {
      id: 'haha',
      description: 'Haha',
      img: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/haha.gif',
      ic: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/haha2.png',
      color: '#f9a825'
    },
    {
      id: 'yay',
      description: 'Yay',
      img: 'https://i.imgur.com/a44ke8c.gif',
      ic: 'https://www.pngfind.com/pngs/m/90-903492_royalty-free-mealegra-facebookreactions-facebook-reaction-facebook-yay.png',
      color: '#e65100'
    },
    {
      id: 'wow',
      description: 'Wow',
      img: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/wow.gif',
      ic: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/wow2.png',
      color: '#e65100'
    },
    {
      id: 'sad',
      description: 'Sad',
      img: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/sad.gif',
      ic: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/sad2.png',
      color: '#e65100'
    },
    {
      id: 'angry',
      description: 'Angry',
      img: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/angry.gif',
      ic: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/angry2.png',
      color: '#e65100'
    }
  ],
  imageSizes: {
    landscape: {width: 1200, height: 630},
    portrait: {width: 630, height: 1200},
    square: {width: 1200, height: 1200}
  },
  goal: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1659517848472.png',
  own_goal: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1659517848322.png',
  yellow_card: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1659517847822.png',
  red_card: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1659517847658.png',
  change_player: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vsports-files/1659517848186.png',
  yellow_red_card: 'https://hcm01.vstorage.vngcloud.vn/v1/AUTH_77cdabdfd5ab49fd896ce6b19d7019dc/vstationfiles/hRkGaUoDjO.png',
  imageTypes: /jpeg|jpg|png|gif|heic|octet-stream/,
  videoTypes: /mov|mpg|mpeg|mp4|wmv|avi|qt|quicktime/,
  statusColors: {
    error: 'rgba(171, 0, 60, 0.8)',
    failed: 'rgba(171, 0, 60, 0.8)',
    pending: 'rgba(240, 173, 1, 0.8)',
    completed: 'rgba(52, 188, 68, 0.8)',
    inreview: 'rgba(22, 117, 206, 0.8)'
  }
}
